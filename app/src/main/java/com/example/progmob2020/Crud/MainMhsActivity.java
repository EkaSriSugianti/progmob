package com.example.progmob2020.Crud;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progmob2020.Adapter.MahasiswaCRUDRecyclerAdapter;
import com.example.progmob2020.Crud.MahasiswaAddActivity;
import com.example.progmob2020.Crud.MahasiswaDeleteActivity;
import com.example.progmob2020.Crud.MahasiswaGetAllActivity;
import com.example.progmob2020.Crud.MahasiswaUpdateActivity;
import com.example.progmob2020.MainActivity;
import com.example.progmob2020.Model.Mahasiswa;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;
import com.example.progmob2020.TrackerActivity;

public class MainMhsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mhs);

        Button btnGet = (Button)findViewById(R.id.btnGet);
        Button btnAdd = (Button)findViewById(R.id.btnNew);
        Button btnEdt = (Button)findViewById(R.id.btnUp);
        Button btnDel = (Button)findViewById(R.id.btnDel);

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaGetAllActivity.class);
                //String url = intent.getExtras().getString("BASE_URL");
                //intent.putExtra("BASE_URL", url);
                //Bundle bundle = getIntent().getExtras();

                startActivity(intent);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaAddActivity.class);
                startActivity(intent);
            }
        });

        btnEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainMhsActivity.this, MahasiswaUpdateActivity.class);
                startActivity(intent);
            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaDeleteActivity.class);
                startActivity(intent);
            }
        });
    }
}