package com.example.progmob2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        EditText edNimCari = (EditText)findViewById(R.id.editTeksNimCari);
        EditText edNamaBaru = (EditText)findViewById(R.id.editTeksNamaBaru);
        EditText edNimBaru = (EditText)findViewById(R.id.editTeksNimBaru);
        EditText edAlamatBaru = (EditText)findViewById(R.id.editTeksAlamatBaru);
        EditText edEmailBaru = (EditText)findViewById(R.id.editTeksEmailBaru);
        Button btnUp = (Button)findViewById(R.id.btnUpdate);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        Intent data = getIntent();
        edNimCari.setText(data.getStringExtra("nim"));
        edNamaBaru.setText(data.getStringExtra("nama"));
        edNimBaru.setText(data.getStringExtra("nim"));
        edAlamatBaru.setText(data.getStringExtra("alamat"));
        edEmailBaru.setText(data.getStringExtra("email"));

        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        edNamaBaru.getText().toString(),
                        edNimBaru.getText().toString(),
                        edNimCari.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        "Kosongkan",
                        "72180219"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data Berhasil Diupdate", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data Gagal Diupdate", Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(MahasiswaUpdateActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });
    }
}