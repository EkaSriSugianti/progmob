package com.example.progmob2020.CrudMatkul;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulUpdateActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);

        EditText edKodeCari = (EditText) findViewById(R.id.editTeksKodeCari);
        EditText edNamaBaru = (EditText) findViewById(R.id.editTeksNamaBaru);
        EditText edKodeBaru = (EditText) findViewById(R.id.editTeksKodeBaru);
        EditText edHariBaru = (EditText) findViewById(R.id.editTeksHariBaru);
        EditText edSesiBaru = (EditText) findViewById(R.id.editTeksSesiBaru);
        EditText edSksBaru = (EditText) findViewById(R.id.editTeksSksBaru);
        Button btnUp = (Button) findViewById(R.id.btnUpdate);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        Intent data = getIntent();
        edKodeCari.setText(data.getStringExtra("kode"));
        edNamaBaru.setText(data.getStringExtra("nama"));
        edKodeBaru.setText(data.getStringExtra("kode"));
        edHariBaru.setText(data.getStringExtra("hari"));
        edSesiBaru.setText(data.getStringExtra("sesi"));
        edSksBaru.setText(data.getStringExtra("sks"));


        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_matkul(
                        edNamaBaru.getText().toString(),
                        edKodeBaru.getText().toString(),
                        Integer.parseInt(edHariBaru.getText().toString()),
                        Integer.parseInt(edSesiBaru.getText().toString()),
                        Integer.parseInt(edSksBaru.getText().toString()),
                        edKodeCari.getText().toString(),
                        "72180219"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(MatkulUpdateActivity.this, "Data Berhasil Diupdate", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(MatkulUpdateActivity.this, "Data Gagal Diupdate", Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(MatkulUpdateActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });
    }
}

