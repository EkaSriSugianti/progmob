package com.example.progmob2020.Cruddosen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.progmob2020.Crud.MahasiswaAddActivity;
import com.example.progmob2020.Crud.MahasiswaDeleteActivity;
import com.example.progmob2020.Crud.MahasiswaGetAllActivity;
import com.example.progmob2020.Crud.MahasiswaUpdateActivity;
import com.example.progmob2020.Crud.MainMhsActivity;
import com.example.progmob2020.R;

public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        Button btnGet = (Button)findViewById(R.id.btnGet);
        Button btnAdd = (Button)findViewById(R.id.btnNew);
        Button btnEdt = (Button)findViewById(R.id.btnUp);
        Button btnDel = (Button)findViewById(R.id.btnDel);

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenGetAllActivity.class);
                //String url = intent.getExtras().getString("BASE_URL");
                //intent.putExtra("BASE_URL", url);
                //Bundle bundle = getIntent().getExtras();

                startActivity(intent);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenAddActivity.class);
                startActivity(intent);
            }
        });

        btnEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainDosenActivity.this, DosenUpdateActivity.class);
                startActivity(intent);
            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenDeleteActivity.class);
                startActivity(intent);
            }
        });
    }
}