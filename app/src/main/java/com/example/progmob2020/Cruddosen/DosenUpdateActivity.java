package com.example.progmob2020.Cruddosen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);

        EditText edNidnCari = (EditText)findViewById(R.id.editTeksNidnCari);
        EditText edNamaBaru = (EditText)findViewById(R.id.editTeksNamaBaru);
        EditText edNidnBaru = (EditText)findViewById(R.id.editTeksNidnBaru);
        EditText edAlamatBaru = (EditText)findViewById(R.id.editTeksAlamatBaru);
        EditText edEmailBaru = (EditText)findViewById(R.id.editTeksEmailBaru);
        EditText edGelarBaru = (EditText)findViewById(R.id.editTeksGelarBaru);
        Button btnUp = (Button)findViewById(R.id.btnUpdate);
        pd = new ProgressDialog(DosenUpdateActivity.this);

        Intent data = getIntent();
        edNidnCari.setText(data.getStringExtra("nidn"));
        edNamaBaru.setText(data.getStringExtra("nama"));
        edNidnBaru.setText(data.getStringExtra("nidn"));
        edAlamatBaru.setText(data.getStringExtra("alamat"));
        edEmailBaru.setText(data.getStringExtra("email"));
        edGelarBaru.setText(data.getStringExtra("gelar"));


        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_dsn(
                        edNamaBaru.getText().toString(),
                        edNidnBaru.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        edGelarBaru.getText().toString(),
                        "Kosongkan",
                        "72180239",
                        edNidnCari.getText().toString()
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(DosenUpdateActivity.this, "Data Berhasil Diupdate", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(DosenUpdateActivity.this, "Data Gagal Diupdate", Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(DosenUpdateActivity.this, MainDosenActivity.class);
                startActivity(intent);
            }
        });
    }
}