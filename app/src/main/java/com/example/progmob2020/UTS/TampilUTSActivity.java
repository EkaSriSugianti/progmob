package com.example.progmob2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.progmob2020.Crud.MainMhsActivity;
import com.example.progmob2020.R;

public class TampilUTSActivity extends AppCompatActivity {

    String isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_u_t_s);


        Button btnOut = (Button) findViewById(R.id.btnLogOut);
        Button btnDsn = (Button) findViewById(R.id.btnDosen);
        Button btnMhs = (Button) findViewById(R.id.btnMhs);
        Button btnMk = (Button) findViewById(R.id.btnMatkul);

        Bundle b = getIntent().getExtras();
        String textHelp = b.getString("help_string");

        SharedPreferences pref = TampilUTSActivity.this.getSharedPreferences("pref_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        btnOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TampilUTSActivity.this, MainUTSActivity.class);
                startActivity(intent);
                isLogin = pref.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                }else{
                    editor.putString("isLogin","0");
                }
                editor.commit();

            }
        });

        btnMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TampilUTSActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });

        /*btnDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UtsLoginMainActivity.this, MainDsnActivity.class);
                startActivity(intent);
            }
        });

        btnMk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UtsLoginMainActivity.this, MainMkActivity.class);
                startActivity(intent);
            }
        });*/
    }
}