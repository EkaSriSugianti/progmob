package com.example.progmob2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Model.Matkul;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

public class MatkulCRUDRecyclerAdapter extends RecyclerView.Adapter<MatkulCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Matkul> mkList;

    public MatkulCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mkList = new ArrayList<>();
    }

    public MatkulCRUDRecyclerAdapter(List<Matkul> mkList) {
        this.mkList = mkList;
    }

    public List<Matkul> getMkList() {
        return mkList;
    }

    public void setMkList(List<Matkul> mkList) {
        this.mkList = mkList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MatkulCRUDRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_matkul,parent,false);
        return new MatkulCRUDRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MatkulCRUDRecyclerAdapter.ViewHolder holder, int position) {
        Matkul k = mkList.get(position);

        holder.tvNama.setText(k.getNama());
        holder.tvKode.setText(k.getKode());
        holder.tvHari.setText(Integer.toString(k.getHari()));
        holder.tvSesi.setText(Integer.toString(k.getSesi()));
        holder.tvSks.setText(Integer.toString(k.getSks()));
    }

    @Override
    public int getItemCount() {
        return mkList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvKode, tvHari, tvSesi, tvSks;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvKode = itemView.findViewById(R.id.tvKode);
            tvHari = itemView.findViewById(R.id.tvHari);
            tvSesi = itemView.findViewById(R.id.tvSesi);
            tvSks = itemView.findViewById(R.id.tvSks);
        }
    }
}

